package com.vega.patient.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vega.patient.service.dao.entity.Patient;
import com.vega.patient.service.dao.repository.PatientRepository;
import com.vega.patient.service.handler.PatientHandler;

@RestController
@RequestMapping(path="/patient")
public class PatientController {
	
	@Autowired
	private PatientHandler patientHandler;

	 @RequestMapping("/hello")
	    public String hello(@RequestParam(value="name", defaultValue="World") String name) {
	        return "Hello " + name;
	    }
	 
	 @PutMapping(path="/add")
		public @ResponseBody String addNewUser (@RequestParam String name) {
		 patientHandler.addNewUser(name);
		 return "Patient added successfully";
		}
	 
		@GetMapping(path="/all")
		public @ResponseBody Iterable<Patient> getAllPatients() {
			return patientHandler.findAllPatients();
		}
	 
	 
}
