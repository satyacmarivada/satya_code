package com.vega.patient.service.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "patient")
public class Patient implements Serializable {

	private static final long serialVersionUID = -8095247065315748158L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column (name="idpatient")
	private Integer patientId;
	
	@Column (name="patient_name")
	private String patientName;
	
	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
}
