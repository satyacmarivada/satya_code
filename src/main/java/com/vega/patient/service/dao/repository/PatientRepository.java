package com.vega.patient.service.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.vega.patient.service.dao.entity.Patient;

public interface PatientRepository extends CrudRepository<Patient, Integer>{

}
