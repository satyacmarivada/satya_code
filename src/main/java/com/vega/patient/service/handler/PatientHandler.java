package com.vega.patient.service.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vega.patient.service.dao.entity.Patient;
import com.vega.patient.service.dao.repository.PatientRepository;

@Service
public class PatientHandler {

	@Autowired
	PatientRepository patientRepository;

	public void addNewUser(String name) {
		Patient patient = new Patient();
		patient.setPatientName(name);
		patientRepository.save(patient);
	}

	public Iterable<Patient> findAllPatients() {
		return patientRepository.findAll();
	}
}
